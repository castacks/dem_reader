#include "dem_reader/dem.h"

/*visualization_msgs::Marker InitializeRayMarker(std::string frame, int id=0){
    visualization_msgs::Marker marker;
    marker.header.frame_id = frame;
    marker.header.stamp = ros::Time();
    marker.ns = "rays";
    marker.frame_locked = true;
    marker.id = id;
    marker.type = visualization_msgs::Marker::LINE_LIST;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = 0.0;
    marker.pose.position.y = 0.0;
    marker.pose.position.z = 0.0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;
    marker.scale.x = 0.1;
    marker.scale.y = 0.1;
    marker.scale.z = 0.1;
    marker.color.a = 0.01; // Don't forget to set the alpha!
    if(id ==0){
        marker.color.r = 1.0;
        marker.color.g = 1.0;
        marker.color.b = 1.0;
    }else{
        marker.ns = "global_rays";
        marker.color.r = 0.0;
        marker.color.g = 0.0;
        marker.color.b = 1.0;
    }
    return marker;
}
*/


int main(int argc, char **argv)
{
    DEMData dem_reader;
    std::string file_path = "/home/ubuntu/global_planning_ws/src/dem_reader/data/ftig/USGS_NED_13_n41w077_IMG/USGS_NED_13_n41w077_IMG.img";
    if(dem_reader.loadFile(0,file_path)){
        bool valid = true;

        std::cout<<"Height::"<<dem_reader.getHeight(40.453081,-76.622685,&valid)<<":: valid::"<<valid;
    }
    else{
        std::cout<<"Could not find the file!";
    }

    /*
    ros::init(argc, argv, "example_semantic_grid");
    ros::NodeHandle n;

    ros::Publisher rays_marker_pub = n.advertise<visualization_msgs::Marker>( "/semantic_map/camera_rays", 0 );

    ros::Rate r(10);
    while(ros::ok()){
    ros::spinOnce();
    r.sleep();
    }
    */
    return 0;
}

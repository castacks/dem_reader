cmake_minimum_required(VERSION 2.8.3)
project(dem_reader)

set(CMAKE_BUILD_TYPE Release)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  roscpp cmake_modules
)


# Declare that this catkin package's runtime dependencies
catkin_package(
    INCLUDE_DIRS include
    LIBRARIES ${PROJECT_NAME} gdal
    CATKIN_DEPENDS roscpp
)



#find_package(OpenCV REQUIRED)

include_directories(include ${catkin_INCLUDE_DIRS})

link_directories(lib ${catkin_LIBRARY_DIRS})


#add_executable(depthFloat2Color src/depthFloat2Color.cpp)
#add_dependencies(depthFloat2Color ${PROJECT_NAME}_generate_messages_cpp)
#target_link_libraries(depthFloat2Color ${catkin_LIBRARIES} ${OpenCV_LIBRARIES}) 


add_library(${PROJECT_NAME}  src/dem.cpp)
target_link_libraries(${PROJECT_NAME} ${catkin_LIBRARIES} gdal)

add_executable(${PROJECT_NAME}_example src/example.cpp)
target_link_libraries(${PROJECT_NAME}_example ${PROJECT_NAME} ${catkin_LIBRARIES})

#############
## Install ##
#############

# all install targets should use catkin DESTINATION variables
# See http://ros.org/doc/api/catkin/html/adv_user_guide/variables.html

## Mark executable scripts (Python etc.) for installation
## in contrast to setup.py, you can choose the destination
# install(PROGRAMS
#   scripts/my_python_script
#   DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )


## Mark other files for installation (e.g. launch and bag files, etc.)
# install(FILES
#   # myfile1
#   # myfile2
#   DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
# )
